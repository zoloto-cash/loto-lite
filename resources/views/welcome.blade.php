@extends('layouts.app')

@section('content')
    <div class="d-flex flex-wrap justify-content-center align-content-center vh-100">

        <div class="m-auto">
            <label for="18old" class="text-white m-0">Вам исполнилось 18 лет ?</label>
            <input type="checkbox" name="18old" id="18old">
        </div>
        <div class="m-auto">
            <label class="text-white">Calc</label>
            <input type="text" name="code" id="code" class="form-control" maxlength="14">
            <div class="calculator">
                <div class="row w-auto m-0">
                    <button class="col-sm-4 key" data-value="1">1</button>
                    <button class="col-sm-4 key" data-value="2">2</button>
                    <button class="col-sm-4 key" data-value="3">3</button>
                </div>
                <div class="row w-auto m-0">
                    <button class="col-sm-4 key" data-value="4">4</button>
                    <button class="col-sm-4 key" data-value="5">5</button>
                    <button class="col-sm-4 key" data-value="6">6</button>
                </div>
                <div class="row w-auto m-0">
                    <button class="col-sm-4 key" data-value="7">7</button>
                    <button class="col-sm-4 key" data-value="8">8</button>
                    <button class="col-sm-4 key" data-value="9">9</button>
                </div>
                <div class="row w-auto m-0">
                    <button class="col-sm-4 key" data-value="c">Сброс</button>
                    <button class="col-sm-4 key" data-value="0">0</button>
                    <button class="col-sm-4 key" data-value="x">X</button>
                </div>
            </div>
        </div>
        <div class="w-100 d-flex justify-content-center mt-5">
            <button id="play" class="btn btn-success w-25">Играть</button>
        </div>
    </div>

    <!-- Button trigger modal -->
    <button id="modalBtn" type="button" class="d-none" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ой !</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="modalText"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть </button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.key').click(function () {

            var value = $(this).data('value');

            switch (value) {
                case 'x':
                    if ($('#code').val().length)
                        $('#code').val($('#code').val().substring(0, $('#code').val().length - 1));
                    break;
                case 'c':
                    $('#code').val('');
                    break;

                default:
                    if ($('#code').val().length < 14)
                        $('#code').val($('#code').val() + value);
            }

        });

        $('#play').click(function () {
            if(!$('#18old').prop('checked')) {
                $('#modalText').text('Подтвердите что Вам исполнилось 18 лет');
                $('#modalBtn').trigger('click');
                return;
            }
            if($('#code').val().length != 14) {
                $('#modalText').text('Введите игровой код (14 цифр)');
                $('#modalBtn').trigger('click');
                return;
            }
        });
    </script>
@endsection
